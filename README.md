# Docker JDK 8

This repo exists to provide a custom version of [`docker:latest`](https://hub.docker.com/_/docker/), having additionally:

* Oracle [JDK8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [gradle](https://gradle.org/)

installed.

## How

[This repository](https://github.com/anapsix/docker-alpine-java) provides all the heavy
lifting to install the JDK. It was used to generate the [`Dockerfile`](./Dockerfile).

To install `gradle`, we use [SDKMan](https://sdkman.io/).